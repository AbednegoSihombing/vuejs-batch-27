// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var strsatu = pertama.substring(0, 5);
var strdua = pertama.substring(12, 19);
var strtiga = kedua.substring(0, 8);
var strempat = kedua.substring(8, 19);
var strempat2 = strempat.toUpperCase();

console.log(strsatu.concat(strdua).concat(strtiga).concat(strempat2))

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var int1 = parseInt(kataPertama)
var int2 = parseInt(kataKedua)
var int3 = parseInt(kataKetiga)
var int4 = parseInt(kataKeempat)

var hasil2 = int1 + (int2 * int3) + int4
console.log(hasil2)

// Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
